# This script listens to window resize events, and scales the game viewport to
# a multiple of its initial size, so that pixels are always 1:1 aspect etc..

extends Node


# Get the initial camera size from the active viewport.
onready var original_size = get_tree().root.get_visible_rect().size


func _ready():
	# Callback for when the game window gets resized.
	get_tree().connect("screen_resized", self, "window_resize")

	get_tree().root.set_attach_to_screen_rect(get_tree().root.get_visible_rect())
	window_resize()

func window_resize():
	# Get the current window size.
	var new_size = OS.window_size

	# Determine which of height or width is the smallest proportion of its
	# original size, so that the new game viewport size won't go outside.
	var width_multiplier = int(new_size.x / original_size.x)
	var height_multiplier = int(new_size.y / original_size.y)
	var smallest_multiplier = min(width_multiplier, height_multiplier)

	# How far the game viewport should be inside of the game window.
	var margin = ((new_size - (original_size * smallest_multiplier)) * 0.5).floor()

	# Resize and reposition the viewport.
	get_tree().root.set_attach_to_screen_rect(Rect2(margin, original_size * smallest_multiplier))

	# Resize the built-in border around the viewport.
	VisualServer.black_bars_set_margins(margin.x, margin.y, margin.x, margin.y)
