extends Node

onready var viewport = get_viewport()
var aspect_ratio: float = 0.0

# Get the initial resolution of the window.
var initial_size = OS.get_window_size()

func _ready():
	# Set the aspect ratio based on the initial resolution.
	# If aspect ratio is something other than 1:1, there will be some slight
	# weirdness when crossing over from landscape to portrait.
	# So YMMV.
	aspect_ratio = initial_size.x / initial_size.y

	# Register a callback for the window resizing event.
	viewport.connect("size_changed", self, "window_resize")
	window_resize()

# This method gets called every time the window resolution changes.
func window_resize():
	# Get the current resolution of the window.
	var current_size = OS.get_window_size()
	
	# Calculate the current aspect ratio.
	var current_aspect_ratio = current_size.x / current_size.y
	
	# Compare the current aspect ratio to the default aspect ratio.
	if current_aspect_ratio > aspect_ratio:
		# If the window is wider than expected,
		# tell the camera to make sure everything from top to bottom is inside the viewport.
		get_tree().set_screen_stretch( SceneTree.STRETCH_MODE_2D, SceneTree.STRETCH_ASPECT_KEEP_HEIGHT, initial_size)

	else:
		# If the window is taller than expected,
		# tell the camera to make sure everything from left to right is inside the viewport.
		get_tree().set_screen_stretch( SceneTree.STRETCH_MODE_2D, SceneTree.STRETCH_ASPECT_KEEP_WIDTH, initial_size)
