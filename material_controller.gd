# This class is responsible for going through everything in the scene on startup
# and swap out every existing material with a specific type of material.
# I wrote it in order to have plain SpatialMaterials assigned in the editor for
# visual clarity, and then swap to a ShaderMaterial that does PS1 style affine
# transformations once in-game.

# Once I had written it, I realised it would enable the player to choose what
# kind of visuals they want via options, with only a few extra lines.

# The code is pretty specific to my use case, but perhaps it can serve as
# inspiration or be adapted to another project.

extends Node

class_name MaterialController


export var shader_material: ShaderMaterial

var material_type = ShaderMaterial #SpatialMaterial


func _ready():
	# Determine material type based on saved settings.
	# This could be commented out if not used.
	var shader_setting = Settings.get_option("shader", "value")
	if shader_setting == "shader":
		material_type = ShaderMaterial
	elif shader_setting == "material":
		material_type = SpatialMaterial

	# Loop through all nodes in the scene and make new materials.
	_set_up_materials()

# Normalise material types.
# Materials can be SpatialMaterial or ShaderMaterial,
# or possibly something else, in the future.
# So we need to create new materials of the chosen type
# when the game starts.
#This might need to go in some sort of late cycle, I don't know.
func _set_up_materials():
	# Generate a list of every textured node in the scene.
	var nodes = get_tree().get_nodes_in_group("material")
	for node in nodes:
		# Get the material assigned to the mesh, if any.
		var material = node.get("material/0")
		if material:
			# Get the texture from the existing material, and create a
			# new material of the correct type, according to material_type.
			var new_material = _make_material_with_texture(
					_get_material_texture(material),
					_get_material_uv_offset(material)
			)

			node.set("material/0", new_material)

func _get_material_uv_offset(material: Material) -> Vector2:
	var uv_offset: Vector2
	if material is SpatialMaterial:
		uv_offset = Vector2(material.uv1_offset.x, material.uv1_offset.y)
	else:
		uv_offset = material.get_shader_param("uv1_offset")
	
	return uv_offset

func _get_material_texture(material: Material) -> Texture:
	var texture: Texture
	if material is SpatialMaterial:
		texture = material.albedo_texture
	else:
		texture = material.get_shader_param("texture_resource")
	
	return texture

func _make_material_with_texture(texture: Texture, uv_offset: Vector2) -> Material:
	if material_type == ShaderMaterial:
		var material = shader_material.duplicate()
		material.set_shader_param("texture_resource", texture)
		material.set_shader_param("uv1_offset", uv_offset)
		return material

	# Left that elif there in case we end up with more material types.
	else: #elif material_type == "spatial":
		var material = SpatialMaterial.new()
		material.albedo_texture = texture
		material.uv1_offset = Vector3(uv_offset.x, uv_offset.y, 0.0)
		return material
